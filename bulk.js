// Buscando o elemento pela classe descrita no sheets e setando o link para aquele elemento.
async function LinkUrlToButton(){
    let sheet = await getJsonFromSheet()
    
    setClassInElement()
    let sheetCells= sheet.feed.entry;

    for(let index = 2 ; index < sheetCells.length; index +=2 ){
        let elements = document.querySelectorAll('.' + sheetCells[index].content.$t);
        let linkToButton = sheetCells[index+1].content.$t;

        for(let element of elements) {
            element.setAttribute('href', linkToButton);
        }

        if (!elements)
            continue;
    }
}

// Função para pegar os dados da tabela 1 do sheet
/**
 * 
 * @param {*} codeSheet 
 * @param {*} pageNumber 
 * @returns 
 */
async function getJsonFromSheet(){
    let indexPage = document.currentScript.getAttribute("indexPage");
    let sheetId = document.currentScript.getAttribute("sheetId");

    let url = `https://spreadsheets.google.com/feeds/cells/${sheetId}/${indexPage}/public/full?alt=json`;
    const sheetResponse = await fetch(url);
    let sheetResponseJson = await sheetResponse.json();

    return sheetResponseJson;
}

// setando a classe "sheetBtnX para todos elementos que são botoes
function setClassInElement(){ 
    let elements = document.querySelectorAll('a');
    let count=1;

    elements.forEach(element => {
        classNameAtual = element.className;
        let btnName= 'sheetBtn' + count + ' ' + classNameAtual;
        element.setAttribute('class', btnName );
        count+=1;
    });
}

setClassInElement();
LinkUrlToButton();
